<?php
session_start();
if (!isset($_SESSION['card_number'])) {
	header ('Location: index.html');
	exit();
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>EFREI BANK ATM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="EFREI BANK ATM">
    <meta name="author" content="Audouin d'Aboville">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/my_style.css">
  </head>  
  
<script type="text/javascript">
	function imprimer_page(){
  	window.print();
	}
</script>
  
  <body>
  
<?php
	include("config.php");
	$base = mysql_connect (HOST, USER, PASS);
	mysql_select_db (BDD, $base);
	
	// Retrieve user data
	$sql = 'SELECT * FROM Account_in WHERE CardNumber="'.mysql_real_escape_string($_SESSION['card_number']).'"';
	$req = mysql_query($sql);
	$user_in = mysql_fetch_array($req);
	
	$sql = 'SELECT * FROM Account_out WHERE CardNumber="'.mysql_real_escape_string($_SESSION['card_number']).'"';
	$req = mysql_query($sql);
	$user_out = mysql_fetch_array($req);
	
	if ($user_in[0]!="")
	{
		$name = htmlspecialchars($user_in[2]);
		$phone = htmlspecialchars($user_in[1]);
		$last_login = htmlspecialchars($user_in[3]);
	}
	else if ($user_out[0]!="")
	{
		$name = htmlspecialchars($user_out[2]);
		$phone = htmlspecialchars($user_out[1]);
		$last_login = htmlspecialchars($user_out[3]);
	}
	else
	{
		echo "<script> alert('PHP-Error') </script>";
		echo "<SCRIPT>document.location.href='logout.php'</SCRIPT>";
	}
	
	// Retrieve amount on user account
	$sql = 'SELECT * FROM Bank_account WHERE Owner_card_number="'.mysql_real_escape_string($_SESSION['card_number']).'"';
	$req = mysql_query($sql);
	$Bank_account = mysql_fetch_array($req);
	
	$amount = htmlspecialchars($Bank_account[2]);
	$last_modification = htmlspecialchars($Bank_account[3]);
?>


    <div class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header" style="width: 100%;">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home.php"><h1> <img src="img/pig_bank.png" width="50" height="50"/> Welcome <?php echo $name; ?></h1></a>
          <a class="navbar-brand" href="logout.php" style="float: right;"><h1> Logout </h1></a>
          <a class="navbar-brand" href="javascript:history.back()" style="float: right;"><h1> Previous </h1></a>
        </div>
      </div>
    </div>


    <div class="container col-lg-8 text-center">
      <div class="jumbotron" style="padding-bottom: 96px;">
        <p class="lead">Informations</p>
        <hr>
        <p>	
        	</br>
			<ul class='lead' style='text-align: left;'>
				<li>Name : <?php echo $name; ?></li>
				<li>Phone : <?php echo $phone; ?></li>
				<li>Last login : <?php echo $last_login; ?></li>
				</br>
				<li>Account amount : £ <?php echo $amount; ?></li>
				<li>Last update : <?php echo $last_modification; ?></li>
			</ul>
			 <input style="float: right;" id="print" name="print" type="button" onclick="imprimer_page()" value="Print a copy" /> 
        </p>
      </div>
    </div> 
    
    <div class="container col-lg-4 text-center">
      <div class="jumbotron">
        <p class="lead">My Transactions</p>
        <hr>
        <p>
				<table class="table table-bordered">
						<thead>
							<tr>
								<th width="50%">ID</th>
								<th width="50%">Date</th>
								<th width="50%">Amount</th>
							</tr>
						</thead>
						<tbody>
						
						<?php
						
						$reponse = mysql_query("SELECT * FROM Transaction WHERE Owner_card_number='".$_SESSION['card_number']."' ORDER BY ID DESC LIMIT 0,4");
 						
 						$i=0;
 				
						while ($donnees = mysql_fetch_array($reponse) )
						{
							$i++;
							echo "<tr>";
							echo "<td>$donnees[0]</td>";
							echo "<td>$donnees[3]</td>";
							echo "<td>$donnees[2]</td>";
							echo "</tr>";
						}
						if ($i == 0)
						{
							echo "<tr>";
							echo "<td>No transaction </td>";
							echo "</tr>";
						} 
				
						?>
							
						</tbody>
					</table>   			
        </p>
      </div>
    </div> 

	<hr>

    <div class="footer">    
       <center> <p>2017 - EFREI BANK ATM | Audouin d'Aboville | Mehdi Petit | Adrien Herbert.</p> </center>
    </div>

    
  </body>
</html>