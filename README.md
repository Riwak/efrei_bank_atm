# A PROPOS #

During this project we had implemented a web interface for an ATM (Automated teller machine)/ GAB (Guichet automatique bancaire).

Online version : https://ad-inc.fr/Autres/EFREI_BANK_ATM/

### VERSION NOTES ###

* V1.0

### MODULES ###

* Security (Adrien Herbert) [md5][sha1]
* Database (Mehdi Petit) [mysql][4 tables]
* Program logic (Audouin d'Aboville) [PHP-web-interface][9 files]

### TECHNO INFOS ###

* PHP / HTML / CSS / Bootstrap Framework
* Database under MySQL 5.5

### Contribution guidelines ###

* Audouin d'Aboville
* Adrien Herbert
* Mehdi Petit

### Recquired tools ###

* Web Server running PHP 5.6.2 or later
* Mysql server running libmysql 5.5.38 or later
* Safari, Chrome, Firefox, Opera web browser

### Installation procedure ###

* Download the zip project and unzip it
* Copy the main folder EFREI_BANK_ATM into the www repository on your web server
* Launch both mysql & web server
* Import the sql (/database/data.sql) file into the phpmyadmin interface
* Check HOST, USER, PASS, BDD entry into the config.php file
* Open your web browser and go to "http://localhost:8888/EFREI_BANK_ATM/"

### Annexes & features ###

* Login process require a digital card number and a password
* The user have only 3 chances to pass through the connection. After that his account is block
* The user can show and print a synthesis of his account
* The user can access his transaction historic
* The user can do a Withdraw
* The user can't do a withdraw if his account is negative or equal to zero
* The user can do a deposit
* Multiple bank support
* SQL Injection security patch
* Password are encrypted in database with hash and MD5 algorithm

### USER TEST ###

*login : 00001234 or 00004321 or 12340000
*password : 1234

### PREVIEW ###

![Capture 2017-03-29 à 23.51.30.png](https://bitbucket.org/repo/jgEBLAo/images/1995039874-Capture%202017-03-29%20%C3%A0%2023.51.30.png)

![Capture 2017-03-29 à 23.52.37.png](https://bitbucket.org/repo/jgEBLAo/images/265733293-Capture%202017-03-29%20%C3%A0%2023.52.37.png)

![Capture 2017-03-29 à 23.52.15.png](https://bitbucket.org/repo/jgEBLAo/images/3363202806-Capture%202017-03-29%20%C3%A0%2023.52.15.png)

![Capture 2017-03-29 à 23.52.05.png](https://bitbucket.org/repo/jgEBLAo/images/1207028307-Capture%202017-03-29%20%C3%A0%2023.52.05.png)