<?php
session_start();
if (!isset($_SESSION['card_number'])) {
	header ('Location: index.html');
	exit();
}
	
	// Check the form submission
	if (isset($_POST['check-out']) && $_POST['check-out'] == 'check-out')
	{
	 	// Check the form value
		if ((isset($_POST['withdraw_number']) && !empty($_POST['withdraw_number']))) 
		{
			include("config.php");
   			$base = mysql_connect (HOST, USER, PASS);
			mysql_select_db (BDD, $base);
			
			$withdraw = htmlspecialchars($_POST['withdraw_number']);

			if(is_numeric($withdraw))
			{
				// Check if the database have a couple login (BDD in)
				$sql = 'SELECT count(*) FROM Account_in WHERE CardNumber="'.mysql_real_escape_string($_SESSION['card_number']).'"';
				$req = mysql_query($sql);
				$user_in = mysql_fetch_array($req);
			
				// Check if the database have a couple login(BDD in)
				$sql = 'SELECT count(*) FROM Account_out WHERE CardNumber="'.mysql_real_escape_string($_SESSION['card_number']).'"';
				$req = mysql_query($sql);
				$user_out = mysql_fetch_array($req);
				
				// Retrieve amount from user account
				$sql = 'SELECT Amount FROM Bank_account WHERE Owner_card_number="'.mysql_real_escape_string($_SESSION['card_number']).'"';
				$req = mysql_query($sql);
				$amount = mysql_fetch_array($req);
				
				$amount=$amount[0]-(abs($withdraw));

				// if user is registered we login it into the service
				if ($user_in[0] == 1 || $user_out[0]== 1)
				{
					$sql = 'UPDATE Bank_account SET Amount="'.mysql_real_escape_string($amount).'" WHERE Owner_card_number="'.mysql_real_escape_string($_SESSION['card_number']).'"';
					mysql_query($sql);
					
					$sql = 'UPDATE Bank_account SET Last_modification=NOW() WHERE Owner_card_number="'.mysql_real_escape_string($_SESSION['card_number']).'"';
					mysql_query($sql);
					
					$sql = 'INSERT INTO Transaction VALUES("", "'.mysql_real_escape_string($_SESSION['card_number']).'", "-'.mysql_real_escape_string($withdraw).'", NOW())';
					mysql_query($sql);
				
					echo "<script> alert('Succeed!') </script>";
					echo "<SCRIPT>document.location.href='home.php'</SCRIPT>";
				
					mysql_free_result($req);
					mysql_close();
				
					exit();
				}
				else
				{
					echo "<script> alert('No account found!') </script>";
					echo "<SCRIPT>document.location.href='home.php'</SCRIPT>";
				}
			}
			else
			{
				echo "<script> alert('Numeric value is required') </script>";
				echo "<SCRIPT>document.location.href='home.php'</SCRIPT>";
			}
		}
		else
		{
			echo "<script> alert('One input missing in the form - 2') </script>";
			echo "<SCRIPT>document.location.href='home.php'</SCRIPT>";
		}
	}
	else 
	{
		echo "<script> alert('One input missing in the form - 1') </script>";
		echo "<SCRIPT>document.location.href='home.php'</SCRIPT>";
	}

?>