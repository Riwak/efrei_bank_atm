<?php
session_start();
if (!isset($_SESSION['card_number'])) {
	header ('Location: index.html');
	exit();
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>EFREI BANK ATM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="EFREI BANK ATM">
    <meta name="author" content="Audouin d'Aboville">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/my_style.css">
  </head>  
  
  <body>
  
  	<?php
  	
  		include("config.php");
   		$base = mysql_connect (HOST, USER, PASS);
		mysql_select_db (BDD, $base);
		
		// Retrieve user data
		$sql = 'SELECT * FROM Account_in WHERE CardNumber="'.mysql_real_escape_string($_SESSION['card_number']).'"';
		$req = mysql_query($sql);
		$user_in = mysql_fetch_array($req);
		
		$sql = 'SELECT * FROM Account_out WHERE CardNumber="'.mysql_real_escape_string($_SESSION['card_number']).'"';
		$req = mysql_query($sql);
		$user_out = mysql_fetch_array($req);
		
		if ($user_in[0]!="")
		{
			$name = htmlspecialchars($user_in[2]);
		}
		else if ($user_out[0]!="")
		{
			$name = htmlspecialchars($user_out[2]);
		}
		else
		{
			echo "<script> alert('PHP-Error') </script>";
			echo "<SCRIPT>document.location.href='logout.php'</SCRIPT>";
		}
		
		// Retrieve amount on user account
		$sql = 'SELECT Amount FROM Bank_account WHERE Owner_card_number="'.mysql_real_escape_string($_SESSION['card_number']).'"';
		$req = mysql_query($sql);
		$amount = mysql_fetch_array($req);
  	
  	?>
   

    <div class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header" style="width: 100%;">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home.php"><h1> <img src="img/pig_bank.png" width="50" height="50"/> Welcome <?php echo $name; ?></h1></a>
          <a class="navbar-brand" href="logout.php" style="float: right;"><h1> Logout </h1></a>
        </div>
      </div>
    </div>


    <div class="container col-lg-4 text-center">
      <div class="jumbotron">
        <img src="img/downloads.png" width="150" height="150"/>
        <p class="lead">Withdraw</p>
        <p>	
			
			<?php 
			
			if ($amount[0]<=0)
			{
				echo "<div class='form-group' style='margin-bottom: 148px;'>";
				echo "</br>";
				echo "<div class='alert alert-danger'>";
  				echo "<strong>Alert!</strong> You can't process this action. Your sold is equal to zero or negative.";
				echo "</div>";
				echo "</div>";
			}
			else
			{
				echo "<form action='check_withdraw.php' method='post' class='form-signin' role='form'>";
				echo "<div class='form-group' style='margin-bottom: 105px;'>";
        		echo "</br>";
				echo "<input type='number' class='form-control' placeholder='Ex : 15.00 £' required='' autofocus='' name='withdraw_number' >";
				echo "</div>";
				echo "<input type='submit' name='check-out' value='check-out' class='btn btn-lg btn-primary btn-block'>";
				echo "</br>";
				echo "</form>";
			}
			
			?>
					
        </p>
      </div>
    </div> 
    
    <div class="container col-lg-4 text-center">
      <div class="jumbotron">
        <img src="img/upload.png" width="150" height="150"/>
        <p class="lead">Deposit</p>
        <p>
        	
        	<form action="check_deposit.php" method="post" class="form-signin" role="form">
        
        		<div class="form-group" style="margin-bottom: 105px;">
        		</br>
				<input type="number" class="form-control" placeholder="Ex : 15.00 £" required="" autofocus="" name="deposit_number" >
				</div>
							
				<input class="btn btn-lg btn-primary btn-block" type="submit" name="check-in" value="check-in">
				</br>	
			
			</form>	
					
        </p>
      </div>
    </div> 
    
    
    <div class="container col-lg-4 text-center">
      <div class="jumbotron">
        <img src="img/money.png" width="150" height="150"/>
        <p class="lead">My account</p>
        <p>
			<?php
			if ($amount[0]>=0)
			{
				echo "<h1 style='margin-bottom: 65px; color: #009051;'> £ $amount[0] </h1>";
			}
			else
			{
				echo "<h1 style='margin-bottom: 65px; color: red;'> £ $amount[0] </h1>";
			}
			?>
					
			</br>			
			<a href="Account_synthesis.php" class="btn btn-lg btn-primary btn-block"/> Account synthesis </a>
			</br>				
        </p>
      </div>
    </div> 
    
	<hr>

    <div class="footer">    
       <center> <p>2017 - EFREI BANK ATM | Audouin d'Aboville | Mehdi Petit | Adrien Herbert.</p> </center>
    </div>

    
  </body>
</html>
